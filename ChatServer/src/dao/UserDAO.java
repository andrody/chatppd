/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.User;
import static java.lang.Math.cos;
import java.util.ArrayList;

/**
 *
 * @author andrody
 */
public class UserDAO {
    
    public static ArrayList<User> users = new ArrayList<User>();
    
    public ArrayList<User> getUsersNearBy(User user){
        
        ArrayList<User> nearByUsers  = new ArrayList<User>();
        for(User u : users) {
            int distancia = Math.abs(Integer.parseInt(user.getCoordenada()) - Integer.parseInt(u.getCoordenada()));
            System.out.println("DISTANCIA: " + distancia + "m" + "- Coord1: " + user.getCoordenada() + ", Coord2 :  "  + u.getCoordenada());
            
            if(!u.getId().equals(user.getId()) && distancia <= 200)  {
               u.setDistancia_metros(distancia);
               nearByUsers.add(u);
            }
        }
        
        return nearByUsers;
    }
    
        
    
    
    public void removeUser(User user){
        for (User u : users) {
            System.out.println("Checando se deve remover usuario");
            if (u.getId().equals(user.getId())) {
                System.out.println("Removendo usuário");
                users.remove(u);
                return;
            }
        }
    }
    
    public void addUser(User user){
        for (User u : users) {
            System.out.println("Checando se usuario " + u.getId() + " é igual ao usuario "  + user.getId() + "");
            if (u.getId().equals(user.getId())) {
                System.out.println("Usuários são os mesmos, atualizando campos");
                u.setName(user.getName());
                u.setCoordenada(user.getCoordenada());
                
                u.historico = user.historico;
                return;
            }
        }
        
        System.out.println("Nenhum usuário encontrado com esse id, adicionando um novo na lista");
        users.add(user);
    }
    
    
    
}
