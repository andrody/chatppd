/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import dao.UserDAO;
import entities.User;
import java.util.ArrayList;
import javax.jws.WebService;

/**
 *
 * @author andrody
 */

@WebService(endpointInterface = "ws.UserWS")
public class UserWSImpl implements UserWS {

    private UserDAO userDAO = new UserDAO();
    
    @Override
    public ArrayList<User> getUsersNearBy(User user) {
        return userDAO.getUsersNearBy(user);
    }

    @Override
    public void removeUser(User user) {
        userDAO.removeUser(user);
    }

    @Override
    public void addUser(User user) {
        userDAO.addUser(user);
    }
    
}
