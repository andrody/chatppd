/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import entities.User;
import java.util.ArrayList;
import javax.jws.*;

/**
 *
 * @author andrody
 */

@WebService
public interface UserWS {
    
    @WebMethod
    public ArrayList<User> getUsersNearBy(User user);
 
    @WebMethod
    public void removeUser(User user);
    
    @WebMethod
    public void addUser(User user);
    
}
