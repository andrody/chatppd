/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import javax.xml.ws.Endpoint;
import ws.UserWSImpl;

/**
 *
 * @author andrody
 */
public class ChatServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            
            Endpoint.publish("http://localhost:4790/ws/user", new UserWSImpl());
            System.out.println("publicado");
            
        }catch(Exception e ){
            System.out.println(e.getMessage());
        }
        
    }
    
}
