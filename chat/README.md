#  Jogo de Tabuleiro Trilha com Sockets

####Dados
Nome: Israel Andrew Alves Feitosa
Curso: Engenharia de Computação S9
Matricula: 20112015020230

####IDE
Product Version: NetBeans IDE 8.1 (Build 201510222201)

####Compilador/Interpretador
Java: 1.8.0_73; Java HotSpot(TM) 64-Bit Server VM 25.73-b02
Runtime: Java(TM) SE Runtime Environment 1.8.0_73-b02

####Sistema
System: Windows 10 version 10.0 running on amd64; Cp1252; en_US (nb)

####APIS (dist/lib)
synthetica 2.20.0.jar
syntheticaSimple2D.jar
seaglasslookandfeel-0.2.jar
