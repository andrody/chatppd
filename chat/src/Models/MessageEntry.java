/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Views.ChatView;
import coordenadas.Coordenada;
import java.io.Serializable;
import java.rmi.RemoteException;
import net.jini.core.entry.Entry;
import net.jini.core.event.RemoteEvent;
import net.jini.core.event.RemoteEventListener;
import net.jini.core.event.UnknownEventException;

/**
 *
 * @author andrody
 */
public class MessageEntry implements Entry {
    public String content;
    public String toUserId;
    public String byUserId;
    
    public MessageEntry() {
    }
    
 
   

   public MessageEntry(String content, String toUserId, String byUserId) {
     this.content = content;
     this.toUserId = toUserId;
     this.byUserId = byUserId;
   }
   
   public MessageEntry(String content, String toUserId) {
     this.content = content;
     this.toUserId = toUserId;
//     this.byUserId = byUserId;
   }

   public String toString() {
     return "Content: " + getContent();// + userID + "Content: " + content;
   }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the toUserId
     */
    public String getToUserId() {
        return toUserId;
    }

    /**
     * @param toUserId the toUserId to set
     */
    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * @return the byUserId
     */
    public String getByUserId() {
        return byUserId;
    }

    /**
     * @param byUserId the byUserId to set
     */
    public void setByUserId(String byUserId) {
        this.byUserId = byUserId;
    }

   
}
