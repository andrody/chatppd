/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import coordenadas.Coordenada;
import net.jini.core.entry.Entry;

/**
 *
 * @author andrody
 */
public class UserEntry implements Entry {
    public String name;
    public Coordenada[] coordenadas = new Coordenada[2];
    
    public UserEntry() {
   }

   public UserEntry(String name, Coordenada latitude, Coordenada longitude) {
     this.name = name;
     coordenadas[0] = latitude;
     coordenadas[1] = longitude;
   }

   public String toString() {
     return "Username: " + name + "Coordenates: " + coordenadas[0].toString() + ", " + coordenadas[1].toString();
   }
}
