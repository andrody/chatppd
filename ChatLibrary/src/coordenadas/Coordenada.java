/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coordenadas;

import java.io.Serializable;

/**
 *
 * @author andrody
 */
public class Coordenada implements Serializable {
    
    private float grau;
    private float minutos;
    private String sentido;

    public Coordenada(float grau, float minutos, String sentido) {
        this.grau = grau;
        this.minutos = minutos;
        this.sentido = sentido;
    }

    public Coordenada() {
    }
    
    
    /**
     * @return the grau
     */
    public float getGrau() {
        return grau;
    }

    /**
     * @param grau the grau to set
     */
    public void setGrau(float grau) {
        this.grau = grau;
    }

    /**
     * @return the minutos
     */
    public float getMinutos() {
        return minutos;
    }

    /**
     * @param minutos the minutos to set
     */
    public void setMinutos(float minutos) {
        this.minutos = minutos;
    }

    /**
     * @return the sentido
     */
    public String getSentido() {
        return sentido;
    }

    /**
     * @param sentido the sentido to set
     */
    public void setSentido(String sentido) {
        this.sentido = sentido;
    }
    
}
