/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import entities.Conversa;
import coordenadas.Coordenada;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author andrody
 */
public class User implements Serializable{
    
    private String id;
    private String name;
    private String coordenada;    
    private boolean myself;
    private int distancia_metros = 0;
    
    
    public Map<String, Conversa> historico = new HashMap<String, Conversa>();
  

    public User(String name, String coordenada) {       
        this.name = name;
        this.coordenada = coordenada;        
    }
    
    
   public User(){
        this.id = "" + ((int) (Math.random() * 100001));
        this.name = "guest + id";
        this.coordenada = null;
        
   }

    @Override
    public String toString() {
        return name + " (" + distancia_metros + "m)";
    }
   
   

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    

    /**
     * @return the myself
     */
    public boolean isMyself() {
        return myself;
    }

    /**
     * @param myself the myself to set
     */
    public void setMyself(boolean myself) {
        this.myself = myself;
    }

    /**
     * @return the distancia_metros
     */
    public int getDistancia_metros() {
        return distancia_metros;
    }

    /**
     * @param distancia_metros the distancia_metros to set
     */
    public void setDistancia_metros(int distancia_metros) {
        this.distancia_metros = distancia_metros;
    }

    /**
     * @return the historico
     */
    public Map<String, Conversa> getHistorico() {
        return historico;
    }

    /**
     * @param historico the historico to set
     */
    public void setHistorico(HashMap<String, Conversa> historico) {
        this.historico = historico;
    }
    
    public void saveMsg(User fromUser, User toUser, String msg){
       String id = "";
       if(fromUser.getId().equals(this.id)){
           id = toUser.getId();
       }        
        
       if(!this.historico.containsKey(id)){
           this.historico.put(id, new Conversa());
       }
       this.historico.get(id).addMsg(id, msg);
    }

    /**
     * @return the coordenada
     */
    public String getCoordenada() {
        return coordenada;
    }

    /**
     * @param coordenada the coordenada to set
     */
    public void setCoordenada(String coordenada) {
        this.coordenada = coordenada;
    }

   
    
}
