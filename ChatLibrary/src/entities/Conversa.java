/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;

/**
 *
 * @author andrody
 */
public class Conversa {
    
    private ArrayList<MensagemDeUsuario> msgsList = new ArrayList<MensagemDeUsuario>();
    
    public void addMsg(String userId, String msg) {              
        this.getMsgsList().add(new MensagemDeUsuario(userId,msg));        
    }

    /**
     * @return the msgsList
     */
    public ArrayList<MensagemDeUsuario> getMsgsList() {
        return msgsList;
    }

    /**
     * @param msgsList the msgsList to set
     */
    public void setMsgsList(ArrayList<MensagemDeUsuario> msgsList) {
        this.msgsList = msgsList;
    }
    
}
